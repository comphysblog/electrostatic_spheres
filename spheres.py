import fenics as fn

mesh = fn.Mesh('geo.xml')
markers = fn.MeshFunction("size_t", mesh, 'geo_physical_region.xml')
boundaries = fn.MeshFunction('size_t', mesh, 'geo_facet_region.xml')
dx = fn.Measure('dx', domain=mesh, subdomain_data=markers)
V = fn.FunctionSpace(mesh, 'CG', 2)

inner_boundary = fn.DirichletBC(V, fn.Constant(10.0), boundaries, 1)
outer_boundary = fn.DirichletBC(V, fn.Constant(1), boundaries, 2)
bcs =[inner_boundary, outer_boundary]

u = fn.TrialFunction(V)
v = fn.TestFunction(V)
a = fn.dot(fn.grad(u), fn.grad(v)) * fn.dx
L = fn.Constant('0') * v * fn.dx
u = fn.Function(V)
fn.solve(a == L, u, bcs)

electric_field = -fn.grad(u)

potentialFile = fn.File('output/potential.pvd')
potentialFile << u

potentialFile = fn.File('output/potential.pvd')
potentialFile << u
e_fieldfile = fn.File('output/e_field.pvd')
e_fieldfile << electric_field

Merge "geo.step";
Physical Surface("inner", 1) = {2};
//+
Physical Surface("outer", 2) = {1};
//+
Physical Volume("vacuum", 7) = {1};
//+
Transfinite Line {4} = 15 Using Progression 1;
//+
Transfinite Line {1} = 30 Using Progression 1;
